# FROM iquiw/alpine-emacs

FROM        alpine:3.18

#  MAINTAINER  Olaf Meeuwissen <paddy-hack@member.fsf.org>
MAINTAINER Joseph Edwards VIII <jedwards8th@gmail.com>

# FROM alpine:3.14
# MAINTAINER Iku Iwasa "iku.iwasa@gmail.com"

RUN apk update && apk add ca-certificates emacs

# WORKDIR /root
# COPY init.el /root/.emacs.d/
# COPY entrypoint.sh /
# ENTRYPOINT [ "/entrypoint.sh" ]
# CMD [ "emacs" ]


# ENV PIPX_HOME=/opt/pipx                                                 \
#     PIPX_BIN_DIR=/usr/local/bin                                         \
#     PIPX_MAN_DIR=/usr/local/share/man                                   \
#     PIPX_OPTS="--pip-args=--no-cache-dir --disable-pip-version-check"
ARG _VERSION

RUN apk add --no-cache                                                  \
	python3                                                         \
	py3-pip                                                     \
	libxml2                                                         \
	libxslt                                                         \
	jpeg                                                         && \
    apk add --no-cache --virtual .build-deps                            \
	gcc                                                             \
	musl-dev                                                        \
	py3-wheel                                                       \
	python3-dev                                                     \
	libxml2-dev                                                     \
	libxslt-dev                                                     \
	jpeg-dev                                                     && \
    CFLAGS="$CFLAGS -L/lib"                                             \
    pip install --no-cache-dir nikola$_VERSION                       && \
    apk del .build-deps

#    rm -rf ~/.cache $PIPX_HOME/.cache $PIPX_HOME/logs                && \
#    find /usr/lib/python3.* $PIPX_HOME                                  \
#	\( -type d -a -name test -o -name tests \)                      \
#	-o \( -type f -a -name '*.pyc' -o -name '*.pyo' \)              \
#	-exec rm -rf '{}' +

RUN apk add --no-cache                                                  \
	libstdc++                                                    && \
    apk add --no-cache --virtual .extra-build-deps                      \
	g++                                                             \
	libffi-dev                                                      \
	musl-dev                                                        \
	py3-wheel                                                       \
	python3-dev                                                     \
	zeromq-dev                                                   && \
    pip install --force --no-cache-dir 'nikola[extras]'$_VERSION     && \
    apk del .extra-build-deps

# && \
#    rm -rf ~/.cache $PIPX_HOME/.cache $PIPX_HOME/logs                && \
#    find /usr/lib/python3.* $PIPX_HOME                                  \
#	\( -type d -a -name test -o -name tests \)                      \
#	-o \( -type f -a -name '*.pyc' -o -name '*.pyo' \)              \
#	-exec rm -rf '{}' +
